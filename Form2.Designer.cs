﻿namespace $safeprojectname$
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.availableCommandsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAnRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAnRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.betaCommandsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editAnRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDataTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schelduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dispozitiveMobileDataSet = new $safeprojectname$.DispozitiveMobileDataSet();
            this.dispozitiveMobileDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.producatoriTableAdapter1 = new $safeprojectname$.DispozitiveMobileDataSetTableAdapters.ProducatoriTableAdapter();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dispozitiveMobileDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dispozitiveMobileDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.availableCommandsToolStripMenuItem,
            this.betaCommandsToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.schelduleToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(926, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // availableCommandsToolStripMenuItem
            // 
            this.availableCommandsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAnRowToolStripMenuItem,
            this.deleteAnRowToolStripMenuItem,
            this.updateTableToolStripMenuItem});
            this.availableCommandsToolStripMenuItem.Name = "availableCommandsToolStripMenuItem";
            this.availableCommandsToolStripMenuItem.Size = new System.Drawing.Size(130, 20);
            this.availableCommandsToolStripMenuItem.Text = "Available commands";
            // 
            // addAnRowToolStripMenuItem
            // 
            this.addAnRowToolStripMenuItem.Name = "addAnRowToolStripMenuItem";
            this.addAnRowToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addAnRowToolStripMenuItem.Text = "Add a row";
            this.addAnRowToolStripMenuItem.Click += new System.EventHandler(this.addAnRowToolStripMenuItem_Click);
            // 
            // deleteAnRowToolStripMenuItem
            // 
            this.deleteAnRowToolStripMenuItem.Name = "deleteAnRowToolStripMenuItem";
            this.deleteAnRowToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteAnRowToolStripMenuItem.Text = "Delete a row";
            // 
            // updateTableToolStripMenuItem
            // 
            this.updateTableToolStripMenuItem.Name = "updateTableToolStripMenuItem";
            this.updateTableToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.updateTableToolStripMenuItem.Text = "Update table";
            this.updateTableToolStripMenuItem.Click += new System.EventHandler(this.updateTableToolStripMenuItem_Click);
            // 
            // betaCommandsToolStripMenuItem
            // 
            this.betaCommandsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editAnRowToolStripMenuItem,
            this.changeDataTypeToolStripMenuItem});
            this.betaCommandsToolStripMenuItem.Name = "betaCommandsToolStripMenuItem";
            this.betaCommandsToolStripMenuItem.Size = new System.Drawing.Size(105, 20);
            this.betaCommandsToolStripMenuItem.Text = "Beta commands";
            // 
            // editAnRowToolStripMenuItem
            // 
            this.editAnRowToolStripMenuItem.Name = "editAnRowToolStripMenuItem";
            this.editAnRowToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.editAnRowToolStripMenuItem.Text = "Edit a row";
            // 
            // changeDataTypeToolStripMenuItem
            // 
            this.changeDataTypeToolStripMenuItem.Name = "changeDataTypeToolStripMenuItem";
            this.changeDataTypeToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.changeDataTypeToolStripMenuItem.Text = "Change data type";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // schelduleToolStripMenuItem
            // 
            this.schelduleToolStripMenuItem.Name = "schelduleToolStripMenuItem";
            this.schelduleToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.schelduleToolStripMenuItem.Text = "Schedule";
            this.schelduleToolStripMenuItem.Click += new System.EventHandler(this.schelduleToolStripMenuItem_Click_1);
            // 
            // dispozitiveMobileDataSet
            // 
            this.dispozitiveMobileDataSet.DataSetName = "DispozitiveMobileDataSet";
            this.dispozitiveMobileDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dispozitiveMobileDataSetBindingSource
            // 
            this.dispozitiveMobileDataSetBindingSource.DataSource = this.dispozitiveMobileDataSet;
            this.dispozitiveMobileDataSetBindingSource.Position = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(120, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Write SQL instruction";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(253, 81);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(368, 24);
            this.textBox3.TabIndex = 7;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(345, 159);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Reset";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(164, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Table name";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(270, 111);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 12;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // producatoriTableAdapter1
            // 
            this.producatoriTableAdapter1.ClearBeforeFill = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(123, 226);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(722, 278);
            this.dataGridView1.TabIndex = 15;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(545, 158);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "Show";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 516);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Menu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dispozitiveMobileDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dispozitiveMobileDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem availableCommandsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAnRowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteAnRowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem betaCommandsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editAnRowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeDataTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.BindingSource dispozitiveMobileDataSetBindingSource;
        private DispozitiveMobileDataSet dispozitiveMobileDataSet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem updateTableToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private DispozitiveMobileDataSetTableAdapters.ProducatoriTableAdapter producatoriTableAdapter1;
        private System.Windows.Forms.ToolStripMenuItem schelduleToolStripMenuItem;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
    }
}