﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace $safeprojectname$
{
    public partial class Form5 : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source = .\BLUECC; Initial Catalog = DispozitiveMobile; Integrated Security=True");
        SqlDataAdapter sda;
        DataTable dt;
        SqlCommandBuilder scb;
        private string Nm;
        public string Passvalue
        {
            get { return Nm; }
            set { Nm = value; }
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            button1.Text = Nm;
        }
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = Nm;
           
            button2.Enabled = true;


        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scb = new SqlCommandBuilder(sda);
            sda.Update(dt);
            MessageBox.Show("Success");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f2 = new Form1();
            f2.Show();
            Form5 f1 = new Form5();
            f1.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button3.Visible = true;
            button4.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            sda = new SqlDataAdapter(@"Select * from Produse2_View;", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            sda = new SqlDataAdapter(@"Select * from Comenzi", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
