create database  DispozitiveMobile;
go
use DispozitiveMobile;

go
create table Clienti(ID_Client char(10)  not null primary key,
					 Log_Client char(20) not null,
					 Pass_Client char(20) null,
                     NumClient char(20) null,
					 PrenClient char(20) null,
                     AdrClient char(50)null,
					 Contact_Client char(50) null,
                     Email_Client char(255)null
                     );

go
create table Depozit
             (
			  ID_Depozit char(10) not null primary key,
			  Nr_Depozit char(10) null,
			  Adr_Depozit char(100) not null,
			  Telefon char(9) null
			  );
go
create table Producatori
             (
			 ID_Prod char(10) primary key not null,
			 Nume_Prod char(20) null,
			 Adresa_Prod char(100) not null,
			 Telefon_Prod char(9) null,
			 Email_Prod char(100)
			 );

go
create table Corp_Dis
(
			 ID_CORP char(10) not null primary key,
			 CORP_SIM			char(50)null,
			 CORP_Display		char(50)null,	 		 
);
go

create table Memory_Dis
(
			 ID_MEM char(10) not null primary key,
			 MEM_Card_slot		char(20)null,
			 MEM_Internal		char(50)null
);
go

create table Camera_Dis
(
			 ID_CAM				char(10) not null primary key,
			 CAM_Primary		char(50)null,
			 CAM_Features		char(100)null,
			 CAM_Video			char(50)null,
			 CAM_Secondary		char(50)null
);
go
create table Dispozitive
             (
			 ID_Dis				char(10) not null primary key,
			 Nume_Dis			char(20) null,
			 Launch_Announced	date null,	
			 ID_CORP			char(10) not null foreign key references Corp_Dis,
			 PLAT_OS			char(50)null,
			 PLAT_CPU			char(50)null,
			 ID_MEM char(10) not null foreign key references Memory_Dis ,
			 ID_CAM				char(10) not null foreign key references Camera_Dis,
			 BATT				char(50)  null,
			 MISC_Price			decimal(8,2) null,
			 ID_Prod			char(10) not null foreign key references Producatori,
			 ID_Depozit			char(10) not null foreign key references Depozit,
			 );

go
create table Angajati
			(
			ID_Ang char(10) not null primary key,
			Log_Ang char(20) null,
			Pass_Ang char(20) null,
			Contact_Ang char(20) null,
			Rasp char(20) null,
			);
go
create table Comenzi(
                    ID_Comanda char(10)  not null primary key ,
                    DataComanda date not null,
					Modif char(50) null,
					ID_Ang char(10) not null foreign key references Angajati(ID_Ang)
                    ); 

go
create table DisComandat(
                            ID_Com char(10) not null primary key,
                            NumarArticol int not null,
                            ID_Dis char(10) not null foreign key references Dispozitive,
                            ID_Client char(10)  not null foreign key references Clienti,
                            ID_Comanda char(10) not null foreign key references Comenzi
                            );

go
Insert into Depozit values
('1001','1','Sciusev 23','54543'),
('1002','21','Iesilor 23','543443'),
('1003','123','Grenoble 23','51143'),
('1004','213','Columna 22','54545'),
('1005','2','Viteazul 25','52243'),
('1006','4','Puskin 23','52233'),
('1007','55','Sciusev 289','544543'),
('1008','66','Moroven 25','54553'),
('1009','69','Vaner 23','54577'),
('10010','777','Cuza-Voda 52','777543');
go

Insert into Producatori values
('2001','Xiaomi','Huang 23','33535','fer@mail.ru'),
('2002','HTC','Huang 21','3223545','fer23@mail.ru'),
('2003','Samsung','Horo 21','34523545','fgy23@mail.ru'),
('2004','Microsoft','California 21','34522345','cal23@mail.ru'),
('2005','Apple','California 27','34235345','app33@mail.ru'),
('2006','Sony','Broadway 27','341114345','sony33@mail.ru'),
('2007','Lenovo','Huager 245','342411145','leno33@mail.ru'),
('2008','Meizu','Huaha 245','11111145','mezio33@mail.ru'),
('2009','ZTE','Chihihi 245','153461145','ZTEo33@mail.ru');
go

Insert into Corp_DIs values
('3001','2','5.00 inch'),
('3002','2','6.00 inch'),
('3003','2','5.20 inch'),
('3004','2','4.70 inch'),
('3005','1','5.50 inch'),
('3006','2','5.50 inch'),
('3007','3','4.00 inch'),
('3008','1','4.50 inch'),
('3009','1','6.70 inch'),
('3010','2','7.00 inch');
go

Insert into Memory_Dis values
('4001','Yes','32GB'),
('4002','No','16GB'),
('4003','No','64GB'),
('4004','Yes','128GB'),
('4005','No','32GB'),
('4006','Yes','8GB'),
('4007','Yes','4GB'),
('4008','No','128GB'),
('4009','Yes','16GB'),
('4010','Yes','2GB');
go

Insert into Camera_Dis values
('5001','13 MP, 4128 x 3096 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','5 MP, 720p'),
('5002','23 MP, 5520 � 4140 pixels',' phase detection autofocus, LED flash','2160p@30fps, 1080p@60fps, 720p@120fps, HDR','5.1 MP, 1080p, HDR'),
('5003','20 MP, 5376 x 3752 pixels', 'autofocus, dual-LED (dual tone) flash','2160p@30fps, 1080p@60fps, 720p@120fps,','4 MP, 1080p@30fps, HDR'),
('5004','8 MP,  3264 x 2448 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','5 MP, 720p'),
('5005','12 MP, 4128 x 3096 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','5 MP, 720p'),
('5006','8 MP,  3264 x 2448 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','6 MP, 720p'),
('5007','13 MP, 4128 x 3096 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','5 MP, 720p'),
('5008','8 MP,  3264 x 2448 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','4 MP, 720p'),
('5009','13 MP, 4128 x 3096 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','5 MP, 720p'),
('5010','12.3 MP, 4608 x 2592 pixels','Geo-tagging, touch focus, face/smile detection, HDR, panorama','1080p@30fps','5 MP, 720p');

go

Insert into Dispozitive values
('6001','RedmiNote2',	'13.aug.15','3001','Android OS, v5.0 (Lollipop)','Octa-core 2.2 GHz Cortex-A53','4001','5001','3060 mAh battery','345.00','2001','1001'),
('6002','Mi4i',			'13.mar.15','3001','Android OS, v5.0 (Lollipop)','Octa-core 2.2 GHz Cortex-A53','4001','5001','3060 mAh battery','650.00','2001','1001'),
('6003','Redmi1s',		'21.mar.15','3001','Android OS, v5.1 (Lollipop)','Octa-core 2.1 GHz Cortex-A53','4001','5001','3160 mAh battery','245.00','2001','1001'),
('6004','M9',			'13.aug.15','3002','Android OS, v5.2 (Lollipop)','Octa-core 2.4 GHz Cortex-A53','4004','5001','2060 mAh battery','225.00','2002','1001'),
('6005','M8',			'13.aug.15','3001','Android OS, v5.1 (Lollipop)','Octa-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','115.00','2002','1001'),
('6006','M7',			'01.aug.15','3001','Android OS, v5.0 (Lollipop)','Octa-core 2.1 GHz Cortex-A53','4001','5001','2360 mAh battery','555.00','2002','1001'),
('6007','Desire 820',	'14.aug.15','3004','Android OS, v5.0 (Lollipop)','Dual-core 2.6 GHz Cortex-A53','4001','5001','4160 mAh battery','675.00','2002','1001'),
('6008','Mi4',			'21.aug.15','3001','Android OS, v5.1 (Lollipop)','Dual-core 2.2 GHz Cortex-A53','4004','5001','1360 mAh battery','775.00','2001','1001'),
('6009','Mi5plus',		'22.aug.15','3001','Android OS, v5.0 (Lollipop)','Quad-core 2.2 GHz Cortex-A53','4001','5001','1960 mAh battery','155.00','2001','1001'),
('6010','Mi5',			'11.aug.15','3005','Android OS, v5.0 (Lollipop)','Quad-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2001','1001'),

('6011','Desire 616',	'13.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5004','3060 mAh battery','345.00','2002','1001'),
('6012','Desire 626',	'13.mar.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','3060 mAh battery','650.00','2002','1001'),
('6013','Desire 728',	'21.mar.15','3001','Android OS, v4.4 (Kitkat)','Octa-core 2.1 GHz Cortex-A53','4001','5004','3160 mAh battery','245.00','2002','1001'),
('6014','Desire 526',	'13.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5004','2060 mAh battery','225.00','2002','1001'),
('6015','Desire 828',	'13.aug.15','3006','Android OS, v4.4 (Kitkat)','Octa-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','115.00','2002','1001'),
('6016','Desire 510',	'01.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5004','2360 mAh battery','555.00','2002','1001'),
('6017','Desire 210',	'14.aug.15','3007','Android OS, v4.4 (Kitkat)','Dual-core 2.6 GHz Cortex-A53','4001','5004','4160 mAh battery','675.00','2002','1001'),
('6018','Desire 500',	'21.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','1360 mAh battery','775.00','2002','1001'),
('6019','Desire 320',	'22.aug.15','3003','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5004','1960 mAh battery','155.00','2002','1001'),
('6020','Desire 516',	'11.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2002','1001'),

('6021','E7',		'13.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','3060 mAh battery','345.00','2003','1001'),
('6022','E5',		'13.mar.15','3003','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','3060 mAh battery','650.00','2003','1001'),
('6023','A7',		'21.mar.15','3003','Android OS, v4.4 (Kitkat)','Octa-core 2.1 GHz Cortex-A53','4004','5001','3160 mAh battery','245.00','2003','1001'),
('6024','A5',		'13.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','2060 mAh battery','225.00','2003','1001'),
('6025','J5',		'13.aug.15','3004','Android OS, v4.4 (Kitkat)','Octa-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','115.00','2003','1001'),
('6026','J2',		'01.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4004','5008','2360 mAh battery','555.00','2003','1001'),
('6027','S6',		'14.aug.15','3005','Android OS, v4.4 (Kitkat)','Dual-core 2.6 GHz Cortex-A53','4001','5001','4160 mAh battery','675.00','2003','1001'),
('6028','S6 Edge',	'21.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 2.2 GHz Cortex-A5','4001','5001','1360 mAh battery','775.00','2003','1001'),
('6029','S5',		'22.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','1960 mAh battery','155.00','2003','1001'),
('6030','Note 4',	'11.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2003','1001'),

('6031','Galaxy tab 3 p5220',	'13.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','3060 mAh battery','345.00','2003','1001'),
('6032','Galaxy tab 4 p5225',	'13.mar.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','3060 mAh battery','650.00','2003','1001'),
('6033','Galaxy tab 4 p5210',	'21.mar.15','3005','Android OS, v4.4 (Kitkat)','Octa-core 2.1 GHz Cortex-A53','4001','5003','3160 mAh battery','245.00','2003','1001'),
('6034','Galaxy tab 4 p5220',	'13.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','2060 mAh battery','225.00','2003','1001'),
('6035','Galaxy tab 4 p5220',	'13.aug.15','3002','Android OS, v4.4 (Kitkat)','Octa-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','115.00','2003','1001'),
('6036','Galaxy tab 4 p5220',	'01.aug.15','3003','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','2360 mAh battery','555.00','2003','1001'),
('6037','Galaxy tab 3 p5210',	'14.aug.15','3005','Android OS, v4.4 (Kitkat)','Dual-core 1.2 GHz Cortex-A53','4001','5001','4160 mAh battery','675.00','2003','1001'),
('6038','Galaxy tab 3 p5220',	'21.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','1360 mAh battery','225.00','2003','1001'),
('6039','Galaxy tab 3 p5222',	'22.aug.15','3006','Android OS, v4.4 (Kitkat)','Quad-core 1.2 GHz Cortex-A5','4001','5001','1960 mAh battery','155.00','2003','1001'),
('6040','Galaxy tab 3 p5200',	'11.aug.15','3001','Android OS, v4.4 (Kitkat)','Quad-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2003','1001'),

('6041','Lumia 950',		'13.aug.15','3002','Microsoft Windows 10','Quad-core 1.2 GHz Cortex-A5','4001','5001','3060 mAh battery','345.00','2004','1001'),
('6042','Lumia 550',		'13.mar.15','3001','Microsoft Windows 10','Quad-core 1.2 GHz Cortex-A5','4001','5001','3060 mAh battery','650.00','2004','1001'),
('6043','Lumia 540',		'21.mar.15','3003','Microsoft Windows 10','Octa-core 2.1 GHz Cortex-A53','4001','5001','3160 mAh battery','245.00','2004','1001'),
('6044','Lumia 430',		'13.aug.15','3001','Microsoft Windows 10','Quad-core 1.2 GHz Cortex-A5','4001','5001','2060 mAh battery','225.00','2004','1001'),
('6045','Lumia 640',		'13.aug.15','3005','Microsoft Windows 10','Octa-core 2.2 GHz Cortex-A53','4004','5001','1260 mAh battery','115.00','2004','1001'),
('6046','Lumia 532',		'01.aug.15','3001','Microsoft Windows 10','Quad-core 1.2 GHz Cortex-A5','4001','5001','2360 mAh battery','555.00','2004','1001'),
('6047','Lumia 435',		'14.aug.15','3006','Microsoft Windows 10','Dual-core 2.6 GHz Cortex-A53','4004','5001','4160 mAh battery','675.00','2004','1001'),
('6048','Lumia 532 XL',		'21.aug.15','3001','Microsoft Windows 10','Quad-core 2.2 GHz Cortex-A5','4001','5001','1360 mAh battery','775.00','2004','1001'),
('6049','Surface ',			'22.aug.15','3001','Microsoft Windows 10','Quad-core 1.2 GHz Cortex-A5','4001','5001','1960 mAh battery','155.00','2004','1001'),
('6050','Surface 2',		'11.aug.15','3005','Microsoft Windows 10','Quad-core 2.2 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2004','1001'),

('6051','6S PLUS',			'13.aug.15','3001','iOS 8','Dual-core 1.4 GHz Typhoon (ARM v8-based)','4001','5001','3060 mAh battery','345.00','2005','1001'),
('6052','6S',				'13.mar.15','3002','iOS 8','Dual-core 1.4 GHz Typhoon (ARM v8-based)','4001','5001','3060 mAh battery','650.00','2005','1001'),
('6053','6',				'21.mar.15','3001','iOS 8','Dual-core 1.4 GHz Typhoon (ARM v8-based)','4001','5001','3160 mAh battery','245.00','2005','1001'),
('6054','5S',				'13.aug.15','3003','iOS 8','Dual-core 1.4 GHz Typhoon (ARM v8-based)','4004','5003','2060 mAh battery','225.00','2005','1001'),
('6055','5',				'13.aug.15','3001','iOS 8','Dual-core 1.4 GHz Typhoon (ARM v8-based)','4001','5001','1260 mAh battery','115.00','2005','1001'),
('6056','4S',				'01.aug.15','3001','iOS 8','Dual-core 1.4 GHz Typhoon (ARM v8-based)','4004','5001','2360 mAh battery','555.00','2005','1001'),
('6057','4',				'14.aug.15','3005','iOS 8','Dual-core 1.3 GHz Cyclone (ARM v8-based)','4001','5001','4160 mAh battery','675.00','2005','1001'),
('6058','Air',				'21.aug.15','3008','iOS 8','Dual-core 1.4 GHz Typhoon (ARM v8-based)','4001','5001','1360 mAh battery','775.00','2005','1001'),
('6059','Air 2 ',			'22.aug.15','3001','iOS 8','Dual-core 1.3 GHz Cyclone (ARM v8-based)','4001','5001','1960 mAh battery','155.00','2005','1001'),
('6060','5C',				'11.aug.15','3001','iOS 8','Dual-core 1.3 GHz Cyclone (ARM v8-based)','4001','5001','1260 mAh battery','125.00','2005','1001'),

('6061','Z5 PREMIUM',		'13.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','3060 mAh battery','345.00','2006','1001'),
('6062','Z5 MINI',			'13.mar.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','3060 mAh battery','650.00','2006','1001'),
('6063','Z5 ',				'21.mar.15','3004','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4004','5001','3160 mAh battery','245.00','2006','1001'),
('6064','Z3+',				'13.aug.15','3006','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','2060 mAh battery','225.00','2006','1001'),
('6065','Z3',				'13.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4004','5001','1260 mAh battery','115.00','2006','1001'),
('6066','Z2',				'01.aug.15','3007','Android OS v6.0 (Marshmallow)','Octa-core 2.2 GHz Cortex-A53','4001','5001','2360 mAh battery','555.00','2006','1001'),
('6067','Z4',				'14.aug.15','3008','Android OS v6.0 (Marshmallow)','Octa-core 2.1 GHz Cortex-A53','4001','5001','4160 mAh battery','675.00','2006','1001'),
('6068','E4',				'21.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.2 GHz Cortex-A53','4001','5001','1360 mAh battery','775.00','2006','1001'),
('6069','E3 ',				'22.aug.15','3004','Android OS v6.0 (Marshmallow)','Octa-core 2.3 GHz Cortex-A53','4001','5001','1960 mAh battery','155.00','2006','1001'),
('6070','E2',				'11.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2006','1001'),

('6071','K3 Note',			'13.aug.15','3004','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4001','5001','3060 mAh battery','345.00','2007','1001'),
('6072','P1',				'13.mar.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','3060 mAh battery','650.00','2007','1001'),
('6073','Vibe X2',			'21.mar.15','3005','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4001','5001','3160 mAh battery','245.00','2007','1001'),
('6074','A5000',			'13.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','2060 mAh battery','225.00','2007','1001'),
('6075','A4000',			'13.aug.15','3003','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4004','5005','1260 mAh battery','115.00','2007','1001'),
('6076','S60',				'01.aug.15','3002','Android OS v6.0 (Marshmallow)','Octa-core 2.2 GHz Cortex-A53','4001','5001','2360 mAh battery','555.00','2007','1001'),
('6077','S90',				'14.aug.15','3001','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4001','5001','4160 mAh battery','675.00','2007','1001'),
('6078','S1',				'21.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.2 GHz Cortex-A53','4001','5006','1360 mAh battery','775.00','2007','1001'),
('6079','A328 ',			'22.aug.15','3005','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4004','5001','1960 mAh battery','155.00','2007','1001'),
('6080','K1',				'11.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2007','1001'),

('6081','MX5',				'13.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.2 GHz Cortex-A53','4001','5001','3060 mAh battery','345.00','2008','1001'),
('6082','M1 Note',			'13.mar.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','3060 mAh battery','650.00','2008','1001'),
('6083','M2 Note',			'21.mar.15','3004','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4001','5001','3160 mAh battery','245.00','2008','1001'),
('6084','MX4 Pro',			'13.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','2060 mAh battery','225.00','2008','1001'),
('6085','MX',				'13.aug.15','3001','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4004','5001','1260 mAh battery','115.00','2008','1001'),
('6086','MX3',				'01.aug.15','3002','Android OS v6.0 (Marshmallow)','Octa-core 2.2 GHz Cortex-A53','4001','5001','2360 mAh battery','555.00','2008','1001'),
('6087','Pro5 Mini',		'14.aug.15','3001','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4001','5007','4160 mAh battery','675.00','2008','1001'),
('6088','M1 Metal',			'21.aug.15','3003','Android OS v6.0 (Marshmallow)','Octa-core 2.2 GHz Cortex-A53','4001','5001','1360 mAh battery','775.00','2008','1001'),
('6089','M1 ',				'22.aug.15','3004','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4004','5001','1960 mAh battery','155.00','2008','1001'),
('6090','Pro5',				'11.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2008','1001'),

('6091','Blade X9',			'13.aug.15','3001','Android OS, v5.1 (Lollipop)','Octa-core 2.2 GHz Cortex-A53','4001','5010','3060 mAh battery','345.00','2009','1001'),
('6092','Blade X5',			'13.mar.15','3002','Android OS, v5.1 (Lollipop)','Octa-core 2.0 GHz Cortex-A53','4001','5010','3060 mAh battery','650.00','2009','1001'),
('6093','Blade X3',			'21.mar.15','3001','Android OS, v5.1 (Lollipop)','Quad-core 1.2 GHz Cortex-A53','4004','5010','3160 mAh battery','245.00','2009','1001'),
('6094','Blade S7',			'13.aug.15','3004','Android OS, v5.1 (Lollipop)','Octa-core 2.0 GHz Cortex-A53','4001','5001','2060 mAh battery','225.00','2009','1001'),
('6095','Nubia Z9',			'13.aug.15','3001','Android OS, v5.1 (Lollipop)','Quad-core 1.2 GHz Cortex-A53','4005','5010','1260 mAh battery','115.00','2009','1001'),
('6096','Blade G',			'01.aug.15','3001','Android OS, v5.1 (Lollipop)','Octa-core 1.7 GHz Cortex-A53','4001','5001','2360 mAh battery','555.00','2009','1001'),
('6097','Blade S6',			'14.aug.15','3005','Android OS, v5.1 (Lollipop)','Quad-core 1.2 GHz Cortex-A53','4001','5010','4160 mAh battery','675.00','2009','1001'),
('6098','Speed',			'21.aug.15','3006','Android OS, v5.1 (Lollipop)','Octa-core 2.2 GHz Cortex-A53','4001','5001','1360 mAh battery','775.00','2009','1001'),
('6099','Axon',				'22.aug.15','3005','Android OS v6.0 (Marshmallow)','Quad-core 1.2 GHz Cortex-A53','4001','5001','1960 mAh battery','155.00','2009','1001'),
('6100','Axon Pro',			'11.aug.15','3001','Android OS v6.0 (Marshmallow)','Octa-core 2.0 GHz Cortex-A53','4001','5001','1260 mAh battery','125.00','2009','1001');

Insert into Angajati values
('7001','Admin','1234','54543','Totala');
Insert into Clienti values
('8001','BlueCC','2211','Cristian','Cernat','Cueaza23','234234','more@mail.ru');


/*CREATE VIEW Produse_View AS Select Nume_Dis as Denumire, Launch_Announced as Anuntat, CORP_SIM as Numar_Simuri,
            CORP_Display as Dimensiune,PLAT_OS as Platforma, MEM_Card_slot as Card_Memorie, MEM_Internal as Memorie_Interna , 
			CAM_Primary as Camera_Primara, CAM_Features as Proprietati, CAM_Video	as Video, CAM_Secondary	as Camera_Secundara,
			 BATT as Baterie, MISC_Price as Pret, Nume_Prod as Producator from Dispozitive,Producatori,Corp_Dis,Memory_Dis,Camera_Dis;

CREATE VIEW Produse3_View AS 
Select Nume_Dis as Denumire, Launch_Announced as Anuntat,CORP_Display as Dimensiune,PLAT_OS as Platforma, MEM_Card_slot as Card_Memorie, MEM_Internal as Memorie_Interna , 
			CAM_Primary as Camera_Primara, BATT as Baterie, MISC_Price as Pret, Nume_Prod as Producator from Dispozitive,Producatori,Corp_Dis,Memory_Dis,Camera_Dis;

*/
SELECT     Nume_Dis as Denumire, Launch_Announced as Anuntat,CORP_Display as Dimensiune,PLAT_OS as Platforma, MEM_Card_slot as Card_Memorie, MEM_Internal as Memorie_Interna , 
			CAM_Primary as Camera_Primara, BATT as Baterie, MISC_Price as Pret, Nume_Prod as Producator          
FROM            Corp_Dis INNER JOIN
                         Dispozitive ON Corp_Dis.ID_CORP = Dispozitive.ID_CORP INNER JOIN
                         Camera_Dis ON Dispozitive.ID_CAM = Camera_Dis.ID_CAM INNER JOIN
                         Memory_Dis ON Dispozitive.ID_MEM = Memory_Dis.ID_MEM INNER JOIN
                         Producatori ON Dispozitive.ID_Prod = Producatori.ID_Prod




/*Select * from Produse2_View;

Select Nume_Dis as Denumire from Dispozitive 
Select * from Dispozitive

	SELECT Nr_Depozit
FROM Depozit
WHERE ID_Depozit IN (
						SELECT ID_Depozit
						FROM Dispozitive
						group by ID_Depozit
	
			 )
create view Una as
(		 
SELECT        Memory_Dis.MEM_Internal, Corp_Dis.CORP_SIM, Corp_Dis.CORP_Display, COUNT(Dispozitive.Nume_Dis) AS Total
FROM            Corp_Dis INNER JOIN
                         Dispozitive ON Corp_Dis.ID_CORP = Dispozitive.ID_CORP INNER JOIN
                         Memory_Dis ON Dispozitive.ID_MEM = Memory_Dis.ID_MEM INNER JOIN
                         Producatori ON Dispozitive.ID_Prod = Producatori.ID_Prod
GROUP BY Memory_Dis.MEM_Internal, Corp_Dis.CORP_SIM, Corp_Dis.CORP_Display
)


*/