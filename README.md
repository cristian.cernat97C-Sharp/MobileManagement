# Project Title

Windows Forms project that uses SQL Server services and a sql database.

![Screenshot_6](/uploads/3de597e872d39aded56b109d7c46e557/Screenshot_6.jpg)
![Screenshot_7](/uploads/32e282d4ebf350227b58eb78f6bbeddf/Screenshot_7.jpg)
![Screenshot_8](/uploads/bfba5f98333e69e216c45a9b4aba39c1/Screenshot_8.jpg)


## Getting Started
Run better in Visual Studio 

### Prerequisites
Change the SQlConnection to the string for your server 
````@"Data Source = localhost; Initial Catalog = Drones; Integrated Security=True"```` 
or
````@"Data Source = .\BLUECC; Initial Catalog = Drones; Integrated Security=True"```` 

where BLUECC is the instance name NOT the server
### Installing


```
 git clone https://gitlab.com/cristian.cernat97C-Sharp/MobileManagement.git
```

## Built With

* [SQLServer 2014](https://www.microsoft.com/en-us/download/details.aspx?id=42299) - JDK
* [WindowsForms] - C# WindowsForms


## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 1.0 

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)