﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace $safeprojectname$
{
    public partial class Form4 : Form
    {
        private string Nm;
        public string Passvalue
        {
            get { return Nm; }
            set { Nm = value; }
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            button9.Text = Nm;
        }

        
        SqlConnection con = new SqlConnection(@"Data Source = .\BLUECC; Initial Catalog = DispozitiveMobile; Integrated Security=True");
        SqlDataAdapter sda;
        DataTable dt;
        SqlCommandBuilder scb;
        public Form4()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            label1.Enabled = true;
            label1.Visible = true;
            label1.Text = "Bun venit" + "NumeAdministrator";
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scb = new SqlCommandBuilder(sda);
            sda.Update(dt);
            MessageBox.Show("Success");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            sda = new SqlDataAdapter(@"Select * from Dispozitive", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            sda = new SqlDataAdapter(@"Select * from Depozit", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            sda = new SqlDataAdapter(@"Select * from Producatori", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            sda = new SqlDataAdapter(@"Select * from Clienti", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            sda = new SqlDataAdapter(@"Select * from Angajati", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            button4.Visible = false;
            button6.Visible = false;
            button1.Visible = true;
            button2.Visible = true;
            button3.Visible = true;
            button5.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = true;
            sda = new SqlDataAdapter(@"Select * from Comenzi", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            button2.Visible = false;
            button3.Visible = false;
            button5.Visible = false;

            button4.Visible = true;
            button6.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label4_Click_1(object sender, EventArgs e)
        {
            
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void logOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 f2 = new Form1();
            f2.Show();
            Form4 f1 = new Form4();
            f1.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            button9.Text = Nm;
            button7.Enabled = true;
            button8.Enabled = true;
            dataGridView1.Enabled = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
